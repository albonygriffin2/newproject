Hi,

My name is Liam Mulvaney. I have created two applications. The Customer API handles the logic all the way down to the database. I have
created the front end using angular 5 with typescript.

For Front end I used:
Bootstrap 4
Font Awesome
Google Fonts (Roboto)

With Node I Installed an angular project using npm install @angular/cli // this creates a template project. I updated Typescript to the
latest version as well. 

Other tools with Angular. 
JQuery
I would have loved to use Lodash... but I didn't have time. So Sorry :( (I wanted to use the find to query the customersList, so that the
front end could tell its users about duplicates)
angular2-image-upload // used to create the upload file wizard :)

My CSS can be found as the styles.css for reference

About the code. I had to upload an image using async mechanisms. Still new to it. I could've perfected the code to include the user as well.
I would've loved to use 'readAsDataURL()' -- this converts your uploaded image for the html img tag, to be viewable.

Hope you like it :)

Oh and I based the design on Google materials. refer to their documentation. I enjoy working with their styles. 

For the Backend, (talking about the common layer), I used entity code first. It is much more powerful, as it gives you total control to your
models. You can create database, and use objects to map data. 

I also used a personalized web server for the images. It is free of charge and easy to use. 'web server for chrome'
I ran this application on google chrome