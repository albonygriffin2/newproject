﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModels.Models
{
    [Serializable]
    [JsonObject(MemberSerialization.OptOut)]
    public class Customer
    {
        public Customer() { }
        [Key]
        public int PK { get; set; }
        [Required(ErrorMessage ="Customer without Customer Number")]
        public int EmployeeNumber { get; set; }
        [Required(ErrorMessage ="Customer without a First Name")]
        public string FirstName { get; set; }
        [Required(ErrorMessage ="Customer without a Surname")]
        public string LastName { get; set; }
        [Required(ErrorMessage ="Customer without a Motto")]
        public string Motto { get; set; }
        [Required(ErrorMessage ="Customer without Hobbies")]
        public string Hobbies { get; set; }
        [Required(ErrorMessage ="Customer without a Locality")]
        public string Locality { get; set; }
        [Required(ErrorMessage ="Customer without the Personal Blog URL")]
        public string PBLogURL { get; set; }
        public DateTime LastUpdated { get; set; }
        [Required(ErrorMessage ="Customer without a photo")]
        public string ImagePathURL { get; set; }

    }
}
