﻿using Common;
using DataModels.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Repository
{
    public class CustomerRepository
    {
        private readonly DataContext _DBContext;

        public CustomerRepository()
        {
            _DBContext = new DataContext(); // Initialize the Context for test
        }

        /// <summary>
        /// returns a list of all the customers from the db
        /// </summary>
        /// <returns>the whole list of customers from the context</returns>
        public List<Customer> GetAllCustomers()
        {
            return _DBContext.Customers.ToList(); 
        }

        public Customer GetCustomerByCode(int code)
        {
            return _DBContext.Customers.SingleOrDefault(x => x.EmployeeNumber == code);
        }

        /// <summary>
        /// removes a collection of customers waiting to be deleted
        /// </summary>
        /// <param name="CustomersToDelete">the customers to delete from the database</param>
        /// <returns></returns>
        public List<Customer> DeleteCustomers(List<Customer> CustomersToDelete)
        {
            _DBContext.Customers.RemoveRange(CustomersToDelete);
            _DBContext.SaveChanges();

            return CustomersToDelete;
        }

        public Customer DeleteCustomer(Customer CustomersToDelete)
        {
            _DBContext.Customers.Remove(CustomersToDelete);
            _DBContext.SaveChanges();

            return CustomersToDelete;
        }

        /// <summary>
        /// adds a collection of customers to the database.
        /// </summary>
        /// <param name="CustomersToAdd">the list of customers to add</param>
        /// <returns>the customers that were added</returns>
        public List<Customer> AddCustomers(List<Customer> CustomersToAdd)
        {
            _DBContext.Customers.AddRange(CustomersToAdd);
            _DBContext.SaveChanges();

            return CustomersToAdd;
        }

        public Customer AddCustomer(Customer Customer)
        {
            _DBContext.Customers.Add(Customer);
            return Customer;
        }

        /// <summary>
        /// Save changes to the database
        /// </summary>
        public void Save()
        {
            _DBContext.SaveChanges();
        }

    }
}
