﻿using DataModels.Models;
using DataModels.Models.DTO;
using ServiceLayer.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace CustomerAPI.Controllers
{
    public class CustomerController : ApiController
    {

        public IHttpActionResult GetAll()
        {
            return Ok(new CustomerService().GetAllCustomers());
        }

        private string ImagePathToCreate()
        {
            if (new CustomerService().GetAllCustomers().Any())
            {
                var CustomerImageID = (new CustomerService().GetAllCustomers().LastOrDefault().CustomerNo * 2).ToString();
                var Path = "C:\\Users\\liamm\\OneDrive\\Desktop\\Development\\to_Submit\\CustomerAPI\\CustomerAPI\\Images\\" + CustomerImageID.ToString() + ".jpg";
                return Path; // this is not as efficient as one may thing
            }
            else
            {
                var CustomerImageID = "FirstImage";
                var Path = "C:\\Users\\liamm\\OneDrive\\Desktop\\Development\\to_Submit\\CustomerAPI\\CustomerAPI\\Images\\" + CustomerImageID + ".jpg";
                return Path; // this is not as efficient as one may thing
            }
        }

        private string ImagePathForDatabase()
        {
            if (new CustomerService().GetAllCustomers().Any())
            {
                var CustomerImageID = (new CustomerService().GetAllCustomers().LastOrDefault().CustomerNo * 2).ToString();
                var Path = "http://127.0.0.1:8887/" + CustomerImageID.ToString() + ".jpg";
                return Path; // this is not as efficient as one may thing
            }
            else
            {
                var CustomerImageID = "FirstImage";
                var Path = "http://127.0.0.1:8887/" + CustomerImageID + ".jpg";
                return Path; // this is not as efficient as one may thing
            }
        }

        public async Task<IHttpActionResult> PostUserImage()
        {
            if (!Request.Content.IsMimeMultipartContent())
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);

            byte[] fileBytes = null;
            var filesReadToProvider = await Request.Content.ReadAsMultipartAsync();

            foreach (var stream in filesReadToProvider.Contents)
            {
                fileBytes = await stream.ReadAsByteArrayAsync();
            }

            try
            {
                
                File.WriteAllBytes(ImagePathToCreate(), fileBytes);
                return Ok(new { success = true, data = "Saved to directory" });
            }
            catch(Exception ex)
            {
                var test = ex.Message;
                return Content(HttpStatusCode.BadRequest, ex.Message);
            }
        }

        [HttpPost]
        public IHttpActionResult Create([FromBody]CustomerDTO CustomerToAdd)
        {
            try
            {
                // need a function to save to file
                var _customers = new CustomerService().CustomerToAdd(CustomerToAdd, ImagePathForDatabase());
                //return Ok(_customers);
                return Ok(new { success = true, data= "Created the Applicant"});
            }
            catch(Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, ex.Message);
            }
        }

        [HttpPost]
        public IHttpActionResult Delete([FromBody]CustomerDTO Customer)
        {
            try
            {
                // need a function to save to file
                var _customers = new CustomerService().CustomerToRemove(Customer);
                return Ok(_customers);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, ex.Message);
            }
        }
    }
}
