﻿using DataModels.Models;
using System.Data.Entity;

namespace Common
{
    public class DataContext: DbContext
    {
        public DataContext(): base("name=ThynkContext")
        {
           // leave empty... you can create a database here... I use hybrid Entity Framework code first. Database is already created.
        }

        public DbSet<Customer> Customers { get; set; }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Customer>()
                .ToTable("tblCustomer", "dbo");// maps the class to the table

            modelBuilder.Entity<Customer>()
                .HasKey(m => m.PK)
                .Property(m => m.PK)
                .HasColumnName("PK");

            modelBuilder.Entity<Customer>()
                .Property(m => m.EmployeeNumber)
                .HasColumnName("EmployeeNum");

            modelBuilder.Entity<Customer>()
                .Property(m => m.FirstName)
                .HasColumnName("Name");

            modelBuilder.Entity<Customer>()
                .Property(m => m.LastName)
                .HasColumnName("Surname");

            modelBuilder.Entity<Customer>()
                .Property(m => m.Motto)
                .HasColumnName("Motto");

            modelBuilder.Entity<Customer>()
                .Property(m => m.Hobbies)
                .HasColumnName("Hobbies");

            modelBuilder.Entity<Customer>()
                .Property(m => m.Locality)
                .HasColumnName("Hometown");

            modelBuilder.Entity<Customer>()
                .Property(m => m.PBLogURL)
                .HasColumnName("PBLogURL");

            modelBuilder.Entity<Customer>()
                .Property(m => m.LastUpdated)
                .HasColumnName("DateGenerated");

            modelBuilder.Entity<Customer>()
                .Property(m => m.ImagePathURL)
                .HasColumnName("ImageURL");

        }
    }
}
