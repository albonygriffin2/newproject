﻿using DataAccess.Repository;
using DataModels.Models;
using DataModels.Models.DTO;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace ServiceLayer.Services
{
    public class CustomerService
    {
        private CustomerRepository _Repositories { get; set; }

        public CustomerService()
        {
            _Repositories = new CustomerRepository();
        }
        public List<CustomerDTO> GetAllCustomers()
        {
            var _customers = _Repositories.GetAllCustomers();
            List<CustomerDTO> convertedCustomers = new List<CustomerDTO>();
            _customers.ForEach(_customer => 
            {
                CustomerDTO DTO = new CustomerDTO { CustomerNo = _customer.EmployeeNumber, FirstName = _customer.FirstName, LastName = _customer.LastName, Hobbies = _customer.Hobbies, ImagePathURL = _customer.ImagePathURL, Locality = _customer.Locality, Motto = _customer.Locality, PBLogURL = _customer.PBLogURL };
                convertedCustomers.Add(DTO);
            });

            return convertedCustomers;
        }

        private List<Customer> GetCustomersToAdd(List<Customer> CustomersFromDB, List<Customer> CustomersFromWebsite)
        {
            var customersToAdd = CustomersFromWebsite.Where(CustomerWebsite => !CustomersFromDB.Any(DBCustomers => CustomerWebsite.EmployeeNumber == DBCustomers.EmployeeNumber));
            if (customersToAdd.Any())
                return customersToAdd.ToList();
            else
                return null; 
        }

        public List<Customer> CustomersToAdd(List<Customer> _customersFromWebsite)
        {
            var customersToAdd = GetCustomersToAdd(_Repositories.GetAllCustomers(), _customersFromWebsite);

            if (customersToAdd != null)
            {
                _Repositories.AddCustomers(customersToAdd);
                _Repositories.Save();
            }
            else
                throw new Exception("Nothing to Add."); 

            return customersToAdd;
        }

        public CustomerDTO CustomerToAdd(CustomerDTO Customer, string ImagePath)
        {
            var _queriedCustomer = _Repositories.GetCustomerByCode(Customer.CustomerNo);
            if (_queriedCustomer == null)
            {
                Customer _Customer = new Customer { EmployeeNumber = Customer.CustomerNo, FirstName = Customer.FirstName, LastName = Customer.LastName, Hobbies = Customer.Hobbies, ImagePathURL = ImagePath, Locality = Customer.Locality, Motto = Customer.Motto, PBLogURL = Customer.PBLogURL, LastUpdated = DateTime.Now };
                _Repositories.AddCustomer(_Customer);
                _Repositories.Save();
                return Customer;
            }
            else
                throw new Exception("Customer already exists");

        }

        private List<Customer> GetCustomersToRemove(List<Customer> CustomersFromDB, List<Customer> CustomersFromWebsite)
        {
            var customersToRemove = CustomersFromDB.Where(DBCustomer => !CustomersFromWebsite.Any(WebCustomer => WebCustomer.EmployeeNumber == DBCustomer.EmployeeNumber));
            if (customersToRemove.Any())
                return customersToRemove.ToList();
            else
                return null;
        }

        public List<Customer> CustomersToRemove(List<Customer> _customerFromWebsite)
        {
            var _customersToRemove = GetCustomersToRemove(_Repositories.GetAllCustomers(), _customerFromWebsite);
            if (_customersToRemove != null)
            {
                _Repositories.DeleteCustomers(_customersToRemove);
                _Repositories.Save();
            }
            else
                throw new Exception("Nothing to Delete.");

            return _customersToRemove; 
        }

        private void DeleteImage(string imagePath)
        {
            var image = imagePath.Split('/');
            var _Path = "C:\\Users\\liamm\\OneDrive\\Desktop\\Development\\to_Submit\\CustomerAPI\\CustomerAPI\\Images\\" + image[3];
            if (File.Exists(_Path))
            {
                try
                {
                    File.Delete(_Path);
                }
                catch(Exception ex)
                {

                }
            }
        }

        public CustomerDTO CustomerToRemove(CustomerDTO Customer)
        {
            var _queriedCustomer = _Repositories.GetCustomerByCode(Customer.CustomerNo);
            if (_queriedCustomer != null)
            {
                DeleteImage(Customer.ImagePathURL);
                //Customer _Customer = new Customer { EmployeeNumber = Customer.CustomerNo, FirstName = Customer.FirstName, LastName = Customer.LastName, Hobbies = Customer.Hobbies, ImagePathURL = ImagePath, Locality = Customer.Locality, Motto = Customer.Motto, PBLogURL = Customer.PBLogURL, LastUpdated = DateTime.Now };
                _Repositories.DeleteCustomer(_queriedCustomer);
                _Repositories.Save();
                return Customer;
            }
            else
                throw new Exception("Customer already exists");

        }

    }
}
