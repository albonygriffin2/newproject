import { Component, OnInit } from '@angular/core';
import {CustomerDTO} from '../Models/CustomerDTO';
import {CustomerService} from '../Services/Customer.service';
import { DomSanitizer } from '@angular/platform-browser';
import * as $ from 'jquery';

@Component({
  selector: 'customer-root',
  templateUrl: '../Templates/customer.component.html',
  styleUrls: []
})


export class CustomerComponent implements OnInit {

  CustomerToAdd: CustomerDTO;
  CustomersList: CustomerDTO[] = []; // populated at runtime with the ngOnInit
  isSuccess: boolean = false; 
  startUpload: boolean = false; 
  Image: any;
  customStyle = 
    {
        selectButton: 
        {
            "color":"white",
            "background-color": "#3F51B5",
            "text-transform": "capitalize"
        },
        clearButton: 
        {
            "color":"white",
            "background-color": "#D50000",
            "text-transform": "capitalize"
        }
    };

    ngOnInit()
    {
        this.GetCustomers();
        this.CustomerToAdd = new CustomerDTO();
        this.CustomersList = [];
    }

  SlideAnimation(divIDToOpen, DivAddActive, divIDToClose, divRemoveActive):void
  {
      $("#"+DivAddActive).addClass("Active");
      $("#"+divIDToOpen).slideDown("slow", function(){});
      $("#"+divRemoveActive).removeClass("Active");
      $("#"+divIDToClose).slideUp("slow", function(){});
  }

  //Constructer will also call the service. 
  constructor(_customer : CustomerDTO, private _customerService: CustomerService, private sanitizer: DomSanitizer)
  {
      this.CustomerToAdd = _customer; // this will be the object to return to the MVC Api =]
  }

  sanitize(url:string){
    return this.sanitizer.bypassSecurityTrustUrl(url);
}

  GetCustomers(): void
  {
      this._customerService.GETALL().subscribe(response => 
        {
            this.CustomersList = response;
            if(this.CustomersList.length == 0)
            {
                this.SlideAnimation('View-Create-Customer', 'Create-Tab', 'View-Customers', 'View-Tab');
            } 
            console.log(response);
        });
  }

  // when upload is called this function binds the image to the object
  onUploadFinished(event):void
  {
      if(this.Image ==undefined)
      {
          this.startUpload = true;
        console.log(event.file);
        this.Image = event.file;

        let myImage = new FormData(); 
        myImage.append("file", this.Image);
        this._customerService.PostImage(myImage).subscribe(
              response  => {
                  console.log(response);
                  if(response.success)
                  {
                      this.isSuccess = true;
                      console.log(this.isSuccess);
                      this.startUpload = false;
                  }

                 },
              error =>  {console.log(error); }
            );
      }

  }

  //when on removed is called by the image upload the image is removed from the object
  onRemoved(event):void
  {
      if(this.Image != null){
        this.isSuccess = false;
        this.Image = undefined;
        console.log(this.CustomerToAdd);
      } 
  }

  /*
  adds a user to the typescript array
  */
  Add()
  {
      let _arraylength = this.CustomersList.length; //gets the initial array size to compare
      
       

          console.log(this.Image);
          if(this.isSuccess)
          {
            this._customerService.Create(this.CustomerToAdd).subscribe(data => {
            console.log(data); 
            this.GetCustomers();
            this.CustomerToAdd = new CustomerDTO();
            this.SlideAnimation('View-Customers', 'View-Tab', 'View-Create-Customer', 'Create-Tab')
            });
          }
        
        //send to mvc
  }

  Delete(Customer: CustomerDTO):void
  {
      console.log(Customer);
      this._customerService.Delete(Customer).subscribe(data => 
        {
            console.log(data);
            this.GetCustomers();
        });
  }


}