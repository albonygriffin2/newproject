import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CustomerDTO } from './Models/CustomerDTO';
import {CustomerComponent} from './Components/customer.component';
import { AppComponent } from './app.component';
import {FormsModule} from '@angular/forms';
import { ImageUploadModule } from "angular2-image-upload";
import {HttpClientModule} from '@angular/common/http';
import { CustomerService } from './Services/Customer.service'

@NgModule({
  declarations: [
    AppComponent, CustomerComponent
  ],
  imports: [
    BrowserModule, FormsModule, ImageUploadModule.forRoot(), HttpClientModule
  ],
  providers: [CustomerDTO, CustomerService], //available on call throughout.
  bootstrap: [AppComponent, CustomerComponent]
})
export class AppModule { }
