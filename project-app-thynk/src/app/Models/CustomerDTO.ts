export class CustomerDTO
{
    public FirstName: string;
    public LastName: string;
    public CustomerNo: number;
    public Motto: string; 
    public Hobbies: string; 
    public Locality: string;
    public PBLogURL: string;
    public ImagePathURL: string;
}