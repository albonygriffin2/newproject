import {Injectable} from '@angular/core';
import { Http, Response,Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';

@Injectable()
export class CustomerService
{
    headers: Headers = new Headers();
    constructor(private _http: Http){}

    Create(model: any): Observable<any> {
        let body = JSON.stringify(model);
        let _headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: _headers });
        return this._http.post('http://localhost:59518/api/customer/create', body, options)
            .map((response: Response) => <any>response.json())
    }

    Delete(model: any): Observable<any> {
        let body = JSON.stringify(model);
        let _headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: _headers });
        return this._http.post('http://localhost:59518/api/customer/delete', body, options)
            .map((response: Response) => <any>response.json())
    }

    PostImage(body: any): Observable<any> {
        let url = 'http://localhost:59518/api/customer/PostUserImage';
        let headers = new Headers();
        headers.set('Content-Type', 'application/octet-stream');
        headers.set('Upload-Content-Type', body.type); 
        let options = new RequestOptions({ headers: this.headers });
        return this._http.post(url, body, options)
                        .map((response: Response) => <any>response.json());
    }

    GETALL(): Observable<any>
    {
        var received = this._http.get('http://localhost:59518/api/customer/getall').map((response: Response) => response.json());
        return received;
    }

}